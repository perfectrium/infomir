<?php

class Calculator
{
    /**
     * @var Order
     */
    private $order;

    /**
     * @var ProductsDiscount[]
     */
    private $discounts;

    /**
     * @var ProductsSetFactory
     */
    private $productsSetFactory;

    /**
     * @param Order $order
     * @param ProductsSetFactory $productsSetFactory
     * @param ProductsDiscount[] $discounts
     */
    public function __construct(ProductsSetFactory $productsSetFactory, Order $order, array $discounts)
    {
        $this->order = $order;
        $this->discounts = $discounts;
        $this->productsSetFactory = $productsSetFactory;
    }

    /**
     * @return float|int
     */
    public function calculate()
    {
        $price = 0;
        $products = $this->order->getAll();

        foreach ($this->discounts as $discount) {
            $productsSetList = $discount->getProductsSet($products, $this->productsSetFactory);
            foreach ($productsSetList as $productsSet) {
                $priceWithoutDiscount = $productsSet->getTotalPriceWithoutDiscount();
                $price += $priceWithoutDiscount - ($priceWithoutDiscount * ($productsSet->getDiscountPercent() / 100));
            }
        }

        foreach ($products as $product) {
            if ($product->isDiscountActivated()) {
                continue;
            }

            $price += $product->getPrice();
        }

        return $price;
    }
}