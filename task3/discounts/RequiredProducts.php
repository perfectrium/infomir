<?php

class RequiredProducts extends ProductsDiscount
{
    /**
     * @param array $requiredProducts Продукты, которые должны быть для получения скидки
     * @param int $discountPercent
     */
    public function __construct(array $requiredProducts, $discountPercent)
    {
        $this->requiredProducts = $requiredProducts;
        $this->discountPercent = $discountPercent;
    }

    /**
     * @param Product[] $products
     * @param ProductsSetFactory $productsSetFactory
     * @return ProductsSet[]
     */
    public function getProductsSet(array $products, ProductsSetFactory $productsSetFactory)
    {
        $requiredProductsTmpFlipped = array_flip($this->requiredProducts);
        $possibleProducts = $this->findPossibleProductsForSet($products);

        $productsSetList = [];
        while (true) {
            $diff = array_diff_key($requiredProductsTmpFlipped, $possibleProducts);
            if (!empty($diff) || empty($possibleProducts)) {
                break;
            }

            $productsSet = $productsSetFactory->create($this->discountPercent);
            foreach ($requiredProductsTmpFlipped as $productType => $index) {
                $product = array_pop($possibleProducts[$productType]);
                $productsSet->add($product);

                if (empty($possibleProducts[$productType])) {
                    unset($possibleProducts[$productType]);
                }
            }

            $productsSetList[] = $productsSet;
        }

        return $productsSetList;
    }

    /**
     * @param Product[] $products
     * @return array
     */
    private function findPossibleProductsForSet(array $products)
    {
        $possibleProducts = [];
        foreach ($products as $index => $product) {
            if ($product->isDiscountActivated()) {
                continue;
            }

            if (array_key_exists($product->getType(), array_flip($this->requiredProducts))) {
                $possibleProducts[$product->getType()][$index] = $product;
            }
        }

        return $possibleProducts;
    }
}