<?php

abstract class ProductsDiscount
{
    /**
     * @var array
     */
    protected $requiredProducts;

    /**
     * @var int
     */
    protected $discountPercent;

    /**
     * @return array
     */
    public function getRequiredProductTypes()
    {
        return $this->requiredProducts;
    }

    /**
     * @return int
     */
    public function getDiscountPercent()
    {
        return $this->discountPercent;
    }

    /**
     * @param Product[] $products
     * @param ProductsSetFactory $productsSetFactory
     * @return ProductsSet[]
     */
    abstract function getProductsSet(array $products, ProductsSetFactory $productsSetFactory);
}