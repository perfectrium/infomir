<?php

require 'Product.php';
require 'Calculator.php';
require 'Order.php';
require 'ProductsSet.php';
require 'ProductsSetFactory.php';
require 'discounts/ProductsDiscount.php';
require 'discounts/OneOfSetsRequired.php';
require 'discounts/QuantityDiscount.php';
require 'discounts/QuantityDiscountOr.php';
require 'discounts/RequiredProducts.php';

$product1 = new Product('A', 80);
$product2 = new Product('B', 20);
$product3 = new Product('B', 20);
$product4 = new Product('C', 30);
$product5 = new Product('D', 50);
$product6 = new Product('E', 50);

$order = new Order();
$order->push($product1);
$order->push($product2);
$order->push($product3);
$order->push($product4);
$order->push($product5);
$order->push($product6);

$discountRules = [
    new RequiredProducts(['A', 'B'], 10),
    new RequiredProducts(['D', 'E'], 5),
    new RequiredProducts(['E', 'F', 'G'], 5),
//    new OneOfSetsRequired(['A'], ['K', 'L', 'M'], 5),
//    new QuantityDiscountOr([
//        new QuantityDiscount(3, 5, ['A', 'C']),
//        new QuantityDiscount(4, 10, ['A', 'C']),
//        new QuantityDiscount(5, 20, ['A', 'C'])
//    ]),
];

$calculator = new Calculator(new ProductsSetFactory(), $order, $discountRules);
echo $calculator->calculate();
