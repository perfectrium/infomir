<?php

class ProductsSetFactory
{
    /**
     * @param int $discount
     * @return ProductsSet
     */
    public function create($discount)
    {
        return new ProductsSet($discount);
    }
}