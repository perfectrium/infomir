<?php

class ProductsSet
{
    /**
     * @var int
     */
    private $discountPercent = 0;

    /**
     * @var Product[]
     */
    private $products = [];

    /**
     * @param int $discountPercent
     */
    public function __construct($discountPercent)
    {
        $this->discountPercent = $discountPercent;
    }

    /**
     * @param Product $product
     */
    public function add(Product $product)
    {
        $product->activateDiscount();
        $this->products[] = $product;
    }

    /**
     * @param Product $product
     * @return bool
     */
    public function contains(Product $product)
    {
        return (in_array($product, $this->products, true));
    }

    /**
     * @return float
     */
    public function getTotalPriceWithoutDiscount()
    {
        return array_reduce($this->products, function ($totalPrice, $product) {
            /** @var Product $product */
            return $totalPrice + $product->getPrice();
        });
    }

    /**
     * @return int
     */
    public function getDiscountPercent()
    {
        return $this->discountPercent;
    }
}