<?php

class Order
{
    /**
     * @var Product[]
     */
    private $products;

    /**
     * @param Product $product
     */
    public function push(Product $product)
    {
        $this->products[] = $product;
    }

    /**
     * @return Product[]
     */
    public function getAll()
    {
        return $this->products;
    }
}