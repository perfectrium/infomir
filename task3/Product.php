<?php

class Product
{
    /**
     * @var string
     */
    private $type;

    /**
     * @var float
     */
    private $price;

    /**
     * @var bool
     */
    private $discountActivated = false;

    public function __construct($type, $price)
    {
        $this->type = $type;
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    public function activateDiscount()
    {
        $this->discountActivated = true;
    }

    /**
     * @return bool
     */
    public function isDiscountActivated()
    {
        return $this->discountActivated;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }
}