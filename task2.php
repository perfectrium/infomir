<?php

$str = "{Пожалуйста|Прост} сделайте так, чтобы это {удивительное|крутое|простое} тестовое предложение {изменялось {быстро|мгновенно} случайным образом|менялось каждый раз}.";
//$str = "{A|B} {A{D|E}|B|C}";
//$str = "{A|B} {A|B}";

/**
 * Ищем в переданной строке первое совпадение скобок и текст внутри него ({*}), первыми ищутся самые внутренние
 * скобки. Далее у копии найденного шаблона удаляются скобки и текст разбивается по спец символу (|). Для каждого текста,
 * который получился после разбивки, создается новая строка и шаблон заменяется на текст. В результате получается
 * несколько строк. Далее снова происходит поиск в каждой из этих строк в цикле пока не заменятся все данные.
 */
class Parser {
    private $parsedText = [];
    private $originStr;

    public function parse($originStr)
    {
        $this->originStr = $originStr;
        $this->parsedText[] = $originStr;

        while (true) {
            $new = [];
            foreach ($this->parsedText as $text) {
                foreach ($this->generate($text) as $item) {
                    $new[] = $item;
                }
            }

            if (empty($new)) {
                break;
            }

            $this->parsedText = $new;
        }

        return $this->parsedText;
    }

    private function generate($originStr) {
        $isFound = preg_match('/{[^{}]+}/ui', $originStr, $matches);
        if (!$isFound) {
            return [];
        }

        $possibleOptions = [];
        $parts = explode('|', mb_substr($matches[0], 1, -1));
        foreach ($parts as $part) {
            $pos = mb_strpos($originStr, $matches[0]);
            if ($pos !== false) {
                $possibleOptions[] = mb_substr_replace($originStr, $part, $pos, mb_strlen($matches[0]));
            }
        }

        return $possibleOptions;
    }
}


$parser = new Parser();
echo implode("<br>", $parser->parse($str));






/**
 * https://gist.github.com/JBlond/942f17f629f22e810fe3
 *
 * @param mixed $string The input string.
 * @param mixed $replacement The replacement string.
 * @param mixed $start If start is positive, the replacing will begin at the start'th offset into string.  If start is negative, the replacing will begin at the start'th character from the end of string.
 * @param mixed $length If given and is positive, it represents the length of the portion of string which is to be replaced. If it is negative, it represents the number of characters from the end of string at which to stop replacing. If it is not given, then it will default to strlen( string ); i.e. end the replacing at the end of string. Of course, if length is zero then this function will have the effect of inserting replacement into string at the given start offset.
 * @return string The result string is returned. If string is an array then array is returned.
 */
function mb_substr_replace($string, $replacement, $start, $length=NULL) {
    if (is_array($string)) {
        $num = count($string);
        // $replacement
        $replacement = is_array($replacement) ? array_slice($replacement, 0, $num) : array_pad(array($replacement), $num, $replacement);
        // $start
        if (is_array($start)) {
            $start = array_slice($start, 0, $num);
            foreach ($start as $key => $value)
                $start[$key] = is_int($value) ? $value : 0;
        }
        else {
            $start = array_pad(array($start), $num, $start);
        }
        // $length
        if (!isset($length)) {
            $length = array_fill(0, $num, 0);
        }
        elseif (is_array($length)) {
            $length = array_slice($length, 0, $num);
            foreach ($length as $key => $value)
                $length[$key] = isset($value) ? (is_int($value) ? $value : $num) : 0;
        }
        else {
            $length = array_pad(array($length), $num, $length);
        }
        // Recursive call
        return array_map(__FUNCTION__, $string, $replacement, $start, $length);
    }
    preg_match_all('/./us', (string)$string, $smatches);
    preg_match_all('/./us', (string)$replacement, $rmatches);
    if ($length === NULL) $length = mb_strlen($string);
    array_splice($smatches[0], $start, $length, $rmatches[0]);
    return join($smatches[0]);
}
