<?php

$menu = [
    'simple item',
    [
        'title' => 'Complex item',
        'items' => [
            'simple item2',
            'simple item3',
            [
                'title' => 'Another complex item',
                'items' => ['...']
            ]
        ]
    ]
];

/**
 * Проходимся по каждому элементу массива, если на входе строка, сохраняем ее во временную переменную,
 * если массив, то берем название блока по ключу "title" и также сохраняем в переменную.
 * Далее выводим отступ перед названием, если это не первый уровень (чтобы посчитать уровень, при каждом рекурсивном
 * вызове увеличиваем некий параметр на единицу и делаем соответствующее кол-во пробелов)
 * Если текущий массив рекурсивно вызываем ф-ю, передаем название текущего раздела и увеличинную глубину
 *
 * @param array $menu
 * @param string $prevPath
 * @param int $depth
 */
function showMenu(array $menu, $prevPath = '', $depth = 0) {
    $showNextLevel = static function ($item, $pathName) use ($depth) {
        (!is_array($item))
            ? null
            : showMenu($item['items'], $pathName, ++$depth)
        ;
    };

    foreach ($menu as $item) {
        $pathName = (is_array($item)) ? "$prevPath/{$item['title']}" : "$prevPath/$item";
        echo str_repeat("\t", $depth), $pathName, "\n";
        $showNextLevel($item, $pathName);
    }
}

echo "<pre>\n";
showMenu($menu);
echo '</pre>';
